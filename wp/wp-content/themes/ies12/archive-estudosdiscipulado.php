<?php
	$args        = array( 'post_type' => 'estudosdiscipulado' );
	$posts_array = get_posts( $args );

 	get_header();
 ?>
 		<div id="content" role="main">
			<section role="page" content="estudos">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row">
					<div class="small-10 small-centered medium-8 medium-uncentered columns">
						<ul class="estudos-grid">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<li>
								<span>Estudo de Célula  | <?php the_author(); ?> | <?php the_date(); ?></span>
								<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
							</li>
							<?php endwhile; else : ?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							<?php endif; ?>
						</ul>
					</div>

					<div class="small-10 small centered medium-4 medium-uncentered columns">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer(); ?>