<?php get_header(); ?>
<?php
	$obj = get_post_type_object( get_post_type( $post->ID ) );
	$obj = $obj->labels;
?>
		<div id="content" role="main">
			<section role="page" content="noticia-single">
				<div class="page-header">
					<div class="row">
						<div class="small-12 columns">
							<h2 class="icon-estudos deep_blue uppercase"><?= $obj->name; ?></h2>
						</div>
					</div>
					<div data-interchange="[<?= get_template_directory_uri() ?>/images/backgrounds/bg-noticias.jpg, (default)]" alt=""></div>
					<div class="panel">
						<div class="row">
							<div class="small-10 small-centered columns">
								<ul data-orbit data-options="bullets:false; slide_number:false; timer:false;">
									<li data-orbit-slide="headline-1">
										<div>
											<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat distinctio maiores?</span>
										</div>
									</li>
									<li data-orbit-slide="headline-2">
										<div>
											<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae laboriosam odio nostrum optio dicta?</span>
										</div>
									</li>
									<li data-orbit-slide="headline-3">
										<div>
											<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae sequi fugiat, illo, perspiciatis.</span>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="small-10 small-centered medium-8 medium-uncentered columns">
						<?php while ( have_posts() ) : the_post(); ?>
						<?php the_post_thumbnail(); ?>
						<h2 class="deep_blue"><?php the_title(); ?></h2>
						<em id="postInfo"><?= the_tags('', '&middot;' ); ?>  | <?php the_author(); ?> | <?php the_date(); ?></em>
						
						<article class="text-justify"><?php the_content(); ?></article>

						<div class="row collapse appends">
							<h5 class="deep_blue block uppercase">sobre o autor</h5>
							<div class="small-3 columns">
								<img data-src="holder.js/130x130/gray/text: Profile \n Picture" alt="">
							</div>
							<div class="small-9 columns">
								<span class="uppercase">pr carlos henrique trote</span><br>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa perspiciatis commodi repellat similique sint, ipsum dolore quis! Nihil inventore eius amet, veritatis asperiores dignissimos ex vero! Vero, assumenda quaerat!
							</div>
						</div>

						<div class="row collapse appends">
							<h5 class="deep_blue block uppercase">você vai gostar também de</h5>
							<ul class="small-block-grid-2 medium-block-grid-4">
								<?php $related_query = new WP_Query(array('post_type' => 'estudoscelula', 'posts_per_page' => 4, 'orderby' => 'rand')); ?>
								<?php if ( $related_query->have_posts() ) : while ( $related_query->have_posts() ) : $related_query->the_post(); ?>
								<!-- post -->
								<li>
									<a href="<?= the_permalink(); ?>">
										<?= the_post_thumbnail( 'thumbnail' ); ?>
										<span><?= the_title(); ?></span>
									</a>
								</li>
								<?php endwhile; ?>
								<?php wp_reset_postdata(); ?>
								<!-- post navigation -->
								<?php else: ?>
								<h4>:(</h4>
								<?php endif; ?>
							</ul>
						</div>
						<?php endwhile; // end of the loop. ?>
					</div>

					<div class="small-10 small-centered medium-4 medium-uncentered columns">
						<?php get_sidebar( get_post_type() ); ?>
					</div>
				</div>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer(); ?>