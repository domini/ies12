<?php
	
	$facebookUri      = get_post_meta( $post->ID, 'leader_facebook' );
	$twitterUri       = get_post_meta( $post->ID, 'leader_twitter' );
	$instagramUri     = get_post_meta( $post->ID, 'leader_instagram' );
	$pastorName       = get_post_meta( $post->ID, 'nome' );
	$pastorBio        = get_post_meta( $post->ID, 'bio' );
	$plusOneName      = get_post_meta( $post->ID, 'plus_one_name' );
	$plusOneBio       = get_post_meta( $post->ID, 'plus_one_bio' );
	$plusOneFacebook  = get_post_meta( $post->ID, 'plus_one_facebook' );
	$plusOneTwitter   = get_post_meta( $post->ID, 'plus_one_twitter' );
	$plusOneInstagram = get_post_meta( $post->ID, 'plus_one_instagram' );
	$args             = array( 'post_type' => 'lideranca', 'leadership' => 'pastor' );
	$posts_array      = get_posts( $args );

	get_header();
 ?>
		<div id="content" role="main">	
			<section role="page" content="single-apostolo">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row">
					<div class="medium-6 columns">
						<?php if (has_post_thumbnail()) the_post_thumbnail( 'full' ); ?>
					</div>

					<div class="medium-6 columns">
						<div class="row collapse">
							<div class="small-12 column">
								<h5 class="orange uppercase"><?= $pastorName[0]; ?></h5>
								<p class="text-justify"><?= $pastorBio[0]; ?></p>
							</div>
						</div>
						<div class="row collapse">
							<div class="small-12 columns">
								<div class="row collapse">
									<div class="small-12 medium-4 columns">
										<strong class="uppercase">Redes Sociais:</strong>
									</div>
									<div class="small-12 medium-8 columns">
										<ul class="inline-list">
											<li><a href="<?= 'http://www.facebook.com/' . $facebookUri[0] ?>" class="social icon-facebook"></a></li>
											<li><a href="<?= 'http://www.twitter.com/' . $twitterUri[0] ?>" class="social icon-twitter"></a></li>
											<li><a href="<?= 'http://www.instagram.com/' . $instagramUri[0] ?>" class="social icon-instagram"></a></li>
										</ul>
									</div>
								</div>
								<hr>
							</div>
						</div>

						<div class="row collapse">
							<div class="small-12 column">
								<h5 class="orange uppercase"><?= $plusOneName[0]; ?></h5>
								<p class="text-justify"><?= $plusOneBio[0]; ?></p>
							</div>
						</div>
						<div class="row collapse">
							<div class="small-12 columns">
								<div class="row collapse">
									<div class="small-12 medium-4 columns">
										<strong class="uppercase">Redes Sociais:</strong>
									</div>
									<div class="small-12 medium-8 columns">
										<ul class="inline-list">
											<li><a href="<?= 'http://www.facebook.com/' . $plusOneFacebook[0] ?>" class="social icon-facebook"></a></li>
											<li><a href="<?= 'http://www.twitter.com/' . $plusOneTwitter[0] ?>" class="social icon-twitter"></a></li>
											<li><a href="<?= 'http://www.instagram.com/' . $plusOneInstagram[0] ?>" class="social icon-instagram"></a></li>
										</ul>
									</div>
								</div>
								<hr>
							</div>
						</div>
						<div class="row collapse">
							<div class="small-12 columns">
								<strong class="uppercase">compartilhe</strong>
								<?php echo do_shortcode('[ssba]'); ?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section role="ministerios-slider" class="yellow block">
				<div class="row">
					<div class="small-10 small-centered medium-12 columns">
						<h2 class="icy uppercase">veja também outros pastores:</h2>
						<div class="ministerios-slider">
							<?php foreach ($posts_array as $lider): ?>
							<div>
								<a class="btn-ministerios" href="<?= get_permalink($lider->ID) ?>">
									<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($lider->ID), 'medium' ); ?>
									<img width="98%" data-lazy="<?= $src[0]; ?>" alt="<?= $lider->post_name; ?>">
									<div>
										<span class="uppercase text-center"><?= $lider->post_title; ?></span>
										<small>veja mais</small>
									</div>
								</a>
							</div>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>