<!doctype html>
<!--[if IE 6]>
<html class-"no-js" id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html class-"no-js" id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class-"no-js" id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html class-"no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?php
			// Print the <title> tag based on what is being viewed.
			global $page, $paged;

			wp_title( '|', true, 'right' );

			// Add the blog name.
			bloginfo( 'name' );

			// Add the blog description for the home/front page.
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " &middot; $site_description";

			// Add a page number if necessary:
			if ( $paged >= 2 || $page >= 2 )
				echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
		?></title>

		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>">
		<!--[if IE 8]>
		<style>-ms-filter: "progid:DXImageTransform.Microsoft.Matrix(M11=1, M12=0, M21=0, M22=1, SizingMethod='auto expand')";</style>
		<![endif]-->
		<script>
			window.themeURL='<?php bloginfo('template_url') ?>';
		</script>
		<?php wp_head(); ?>		
	</head>
	<body class="cbp-spmenu-push">
		<section role="offcanvas-nav">
			<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
				<h3>Menu</h3>
				<a href="#">A Igreja</a>
				<a href="#">Motícias</a>
				<a href="#">Mídias</a>
				<a href="#">TV El Shaddai</a>
				<a href="#">Estudos</a>
				<a href="#">Agenda</a>
				<a href="#">Contato</a>
			</nav>
		</section>

		<!--[if lt IE 8]>
			<p class="browsehappy">Você está utilizando um navegador <strong>ultrapassado</strong>. Por favor <a href="http://browsehappy.com/">atualize</a> para ter uma experiência melhor.</p>    
		<![endif]-->

		<header>
			<div id="topbar" role="contactinfo">
				<div class="row">
					<div class="hide-for-small-only medium-7 columns">
						<ul class="inline-list">
							<li>
								<a class="icon-mail text" href="#">contato@ies.com</a>
							</li>
							<li>
								<span class="icon-fone text">+55 11 2068 4020 | 2068 9285</span>
							</li>
						</ul>
					</div>
					<div class="small-12 medium-4 medium-offset-1 columns">
						<div class="icon-socialicon" role="social-media">
							<ul>
								<li><a href="#" class="social icon-youtube"></a></li>
								<li><a href="#" class="social icon-flicker"></a></li>
								<li><a href="#" class="social icon-googleplus-01"></a></li>
								<li><a href="#" class="social icon-facebook"></a></li>
								<li><a href="#" class="social icon-twitter"></a></li>
								<li><a href="#" class="social icon-instagram"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="main-nav" role="navigation">
				<div class="row">
					<div class="small-2 small-push-1 medium-push-0 columns">
						<h1 class="logo">Igreja El Shaddai &middot; Comunidade Cristã</h1>
					</div>

					<div class="show-for-small-only small-3 small-push-7 columns">
						<ul class="inline-list small-nav">
							<li>
								<a href="#" class="btnSearch icon-pesquisa"></a>
							</li>
							<li>
								<a id="offcanvasMenu" class="icon-calendario-lista" href="#"></a>
							</li>
						</ul>
					</div>
					<div class="hide-for-small-only medium-10 columns">
						<nav>
							<ul class="inline-list">
								<li><a href="<?= home_url('/'); ?>">Home</a></li>
								<li>
									<a data-custom-dropdown="content-drop-igreja" href="#">Igreja El Shaddai</a>
								</li>
								<li><a href="<?= get_post_type_archive_link('noticias'); ?>">Notícias</a></li>
								<li>
									<a data-custom-dropdown="content-drop-midias" href="#">Mídias</a>
								</li>
								<li><a href="//celulas.ies12.com">Células</a></li>
								<li>
									<a data-custom-dropdown="content-drop-estudos" href="#">Estudos</a>
								</li>
								<li><a href="<?= get_post_type_archive_link( 'eventos' ); ?>">Agenda</a></li>
								<li><a href="<?= get_permalink( get_page_by_title('contato') ); ?>">Contato</a></li>
								<li><a href="#" class="btnSearch icon-pesquisa"></a></li>
							</ul>
						</nav>
					</div>
					
					<?php get_search_form(); ?>
				</div>
			</div>
		</header>

		<div class="container-dropdown" role="dropdown">
			<div id="content-drop-igreja" data-custom-dropdown-content class="custom-dropdown">
				<div class="wrapper">
					<div class="row">
						<div class="small-12 columns">
							<ul class="medium-block-grid-5">
								<li class="category">
									<span class="blue uppercase">sobre</span>
									<ul>
										<li><a href="<?= get_permalink( get_page_by_title( 'historia' ) ); ?>">História</a></li>
										<li>
											<a href="<?= get_post_type_archive_link( 'igrejas' ); ?>">Igrejas</a>
											<ul>
												<li><a href="<?php printf(get_post_type_archive_link( 'igrejas' ) . '%s', 'sede') ?>">Sede</a></li>
												<li><a href="<?= get_post_type_archive_link( 'igrejas' ); ?>">Filiais</a></li>
												<li><a href="<?= get_post_type_archive_link( 'igrejas' ); ?>?tipo=cobertura">Cobertura</a></li>
											</ul>
										</li>
										<li><a href="<?= get_page_link( get_page_by_title( 'Visão Celular M12' ) ); ?>">Visão Celular no modelo<br>dos 12</a></li>
									</ul>
								</li>
								<li class="category">
									<span class="orange uppercase">liderança</span>
									<ul>
										<li><a href="<?php printf(get_post_type_archive_link( 'lideranca' ) . '%s', 'apostolo-fabio-abbud') ?>">Apóstolo Fábio Abbud</a></li>
										<li><a href="<?php printf(get_post_type_archive_link( 'lideranca' ) . '%s', 'apostola-claudia-abbud') ?>">Apóstola Cláudia Abbud</a></li>
										<li><a href="<?= get_post_type_archive_link( 'lideranca' ); ?>">Pastores</a></li>
									</ul>
								</li>
								<li class="category">
									<span class="green uppercase">ministérios</span>
									<ul>
									<?php
										$ministerios_array = get_posts( array('post_type' => 'ministerio') );
										foreach ($ministerios_array as $ministerio): ?>
										<li><a href="<?= get_permalink( $ministerio->ID ); ?>"><?= get_the_title( $ministerio->ID ); ?></a></li>
									<?php endforeach ?>
									</ul>
								</li>
								<li class="category">
									<span class="bluey uppercase">redes</span>
									<ul>
									<?php
										$redes_array = get_posts( array('post_type' => 'redes') );
										foreach ($redes_array as $rede): ?>
										<li><a href="<?= get_permalink( $rede->ID ); ?>"><?= get_the_title( $rede->ID ); ?></a></li>
									<?php endforeach ?>
									</ul>
								</li>
								<li class="category">
									<span class="red uppercase">células</span>
									<ul>
										<li><a href="//celulas.ies12.com" target="_blank">Buscador de Células</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div id="content-drop-midias" data-custom-dropdown-content class="custom-dropdown">
				<section role="medias">
					<div class="row">
						<div class="small-10 small-centered medium-12 columns">
							<ul class="small-block-grid-1 medium-block-grid-3">
								<li>
									<a href="//www.youtube.com/ies12com">
										<img src="<?= get_template_directory_uri(); ?>/images/midia_videos_menu.jpg" alt="">
										<div class="block red">
											<i class="icon-video"></i>
											<span class="uppercase">veja mais</span>
										</div>
									</a>
								</li>
								<li>
									<a href="//soundcloud.com/ies12">
										<img src="<?= get_template_directory_uri(); ?>/images/midia_audios_menu.jpg" alt="">
										<div class="block yellow">
											<i class="icon-audio"></i>
											<span class="uppercase">veja mais</span>
										</div>
									</a>
								</li>
								<li>
									<a href="//www.flickr.com/photos/ies12/sets/">
										<img src="<?= get_template_directory_uri(); ?>/images/midia_fotos_menu.jpg" alt="">
										<div class="block pinky">
											<i class="icon-foto"></i>
											<span class="uppercase">veja mais</span>
										</div>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</section>
			</div>

			<div id="content-drop-estudos" data-custom-dropdown-content class="custom-dropdown">
				<div class="row">
					<div class="small-10 small-centered medium-12 columns">
						<ul class="small-block-grid-1 medium-block-grid-2">
							<li>
								<a href="<?= get_post_type_archive_link('estudoscelula'); ?>">
									<img class="th radius" src="<?= get_template_directory_uri(); ?>/images/menu_estudos_celula.jpg" alt="">
									<strong>Estudos de Célula</strong>
								</a>
							</li>
							<li>
								<a href="<?= get_post_type_archive_link('estudosdiscipulado'); ?>">
									<img class="th radius" src="<?= get_template_directory_uri(); ?>/images/menu_estudos_discipulado.jpg" alt="">
									<strong>Estudos de Discipulado</strong>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>