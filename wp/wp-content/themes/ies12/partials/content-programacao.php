<?php
	$posts_array = new WP_Query( array( 'post_type' => 'redes' ) );
?>
		<section role="programacao">
			<div class="row">
				<div class="small-12 columns">
					<h2 class="icon-agenda uppercase">programação</h2>
				</div>
				<div class="small-12 columns">
					<ul class="inline-list">
						<li class="uppercase">
							<a href="#">cultos</a>
							<span>Todos os Domingos - às 9:30 &middot; 17:00 &middot; 19:30</span>
						</li>
						<?php if ( $posts_array->have_posts() ) : while ( $posts_array->have_posts() ) : $posts_array->the_post(); ?>
						<!-- post -->
						<?php switch (get_the_title()) {
							case 'Mulheres':
								$class = 'pinky';
								break;

							case 'Empresários':
								$class = 'blue';
								break;

							case 'Jovens':
								$class = 'orange';
								break;

							case 'Crianças':
								$class = 'bluey';
								break;

							case ( preg_match('/isaque e rebeque/i', get_the_title()) ? true : false ):
								$class = 'red';
								break;
							
							default:
								$class = 'purple';
								break;
						} ?>
						<li class="<?= $class ?> uppercase">
							<a class="<?= $class ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							<span><?= get_field( 'ensaios' ) ?></span>
						</li>
						<?php endwhile; ?>
						<!-- post navigation -->
						<?php else: ?>
						<!-- no posts found -->
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</section>
