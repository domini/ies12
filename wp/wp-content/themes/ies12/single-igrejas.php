<?php
	
	$allMetas = get_post_meta( $post->ID );
	get_header();
 ?>
		<div id="content" role="main">	
			<section role="page" content="single-igreja">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row collapse">
					<?php the_title('<h1 class="uppercase orange">', '<small>Igreja El Shaddai Comunidade Cristã</small></h1>', true ); ?>
					<hr>

					<div class="small-10 small-centered medium-12 column">
						<p>
							<i class="huge lead orange icon-perfil-pastor"></i>
							<?php $posts = get_field('lideranca'); $i = 1; if( $posts ): foreach( $posts as $p ): ?>
								<a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a><?php echo ( $i < sizeof($posts) ) ? ', ' : null ?>
							<?php $i++; endforeach; endif; ?>
						</p>
					</div>

					<div class="small-10 small-centered medium-12 column">
						<p>
							<i class="huge lead orange icon-endereco"></i>
							<?= get_field('endereco_completo'); ?>
						</p>
					</div>

					<div class="small-10 small-centered medium-12 column">
						<p>
							<i class="huge lead orange icon-fone"></i>
							<?= get_field('telefone'); ?>
						</p>
					</div>

					<div class="small-10 small-centered medium-12 column panel">
						<?php echo do_shortcode('[ssba]'); ?>
					</div>
				</div>

				<article>
					<?php 
						$location = get_field('mapa');
						if( !empty($location) ): ?>
						<div class="acf-map">
							<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
						</div>
					<?php endif; ?>
				</article>
			</section>

			<style type="text/css">

			.acf-map {
				width: 100%;
				height: 400px;
				border: #ccc solid 1px;
				margin: 20px 0;
			}

			</style>
			<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
			<script type="text/javascript">
			(function($) {

			/*
			*  render_map
			*
			*  This function will render a Google Map onto the selected jQuery element
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	4.3.0
			*
			*  @param	$el (jQuery element)
			*  @return	n/a
			*/

			function render_map( $el ) {

				// var
				var $markers = $el.find('.marker');

				// vars
				var args = {
					zoom		: 16,
					center		: new google.maps.LatLng(0, 0),
					mapTypeId	: google.maps.MapTypeId.ROADMAP,
					styles      : [{"featureType":"administrative","stylers":[{"visibility":"off"}]},{"featureType":"poi","stylers":[{"visibility":"simplified"}]},{"featureType":"road","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","stylers":[{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"visibility":"off"}]},{"featureType":"road.local","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"color":"#5f94ff"},{"lightness":26},{"gamma":5.86}]},{},{"featureType":"road.highway","stylers":[{"weight":0.6},{"saturation":-85},{"lightness":61}]},{"featureType":"road"},{},{"featureType":"landscape","stylers":[{"hue":"#0066ff"},{"saturation":74},{"lightness":100}]}]
				};

				// create map	        	
				var map = new google.maps.Map( $el[0], args);

				// add a markers reference
				map.markers = [];

				// add markers
				$markers.each(function(){

					add_marker( $(this), map );

				});

				// center map
				center_map( map );

			}

			/*
			*  add_marker
			*
			*  This function will add a marker to the selected Google Map
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	4.3.0
			*
			*  @param	$marker (jQuery element)
			*  @param	map (Google Map object)
			*  @return	n/a
			*/

			function add_marker( $marker, map ) {

				// var
				var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

				// create marker
				var marker = new google.maps.Marker({
					position	: latlng,
					map			: map
				});

				// add to array
				map.markers.push( marker );

				// if marker contains HTML, add it to an infoWindow
				if( $marker.html() )
				{
					// create info window
					var infowindow = new google.maps.InfoWindow({
						content		: $marker.html()
					});

					// show info window when marker is clicked
					google.maps.event.addListener(marker, 'click', function() {

						infowindow.open( map, marker );

					});
				}

			}

			/*
			*  center_map
			*
			*  This function will center the map, showing all markers attached to this map
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	4.3.0
			*
			*  @param	map (Google Map object)
			*  @return	n/a
			*/

			function center_map( map ) {

				// vars
				var bounds = new google.maps.LatLngBounds();

				// loop through all markers and create bounds
				$.each( map.markers, function( i, marker ){

					var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

					bounds.extend( latlng );

				});

				// only 1 marker?
				if( map.markers.length == 1 )
				{
					// set center of map
					map.setCenter( bounds.getCenter() );
					map.setZoom( 16 );
				}
				else
				{
					// fit to bounds
					map.fitBounds( bounds );
				}

			}

			/*
			*  document ready
			*
			*  This function will render each map when the document is ready (page has loaded)
			*
			*  @type	function
			*  @date	8/11/2013
			*  @since	5.0.0
			*
			*  @param	n/a
			*  @return	n/a
			*/

			$(document).ready(function(){

				$('.acf-map').each(function(){

					render_map( $(this) );

				});

			});

			})(jQuery);
			</script>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>