<?php get_header() ?>
<?php
	
	$allMetas    = get_post_meta($post->ID);

	$args        = array( 'post_type' => 'redes' );
	$posts_array = get_posts( $args );

 ?>
		<div id="content" role="main">	
			<section role="page" content="redes">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row">
					<div class="medium-6 columns">
						<?= get_the_post_thumbnail( $post->ID, 'large' ); ?>
					</div>

					<div class="medium-6 columns">
						<div class="row collapse">
							<div class="small-12 column">
								<h5 class="greeny">Rede de <?= $post->post_title; ?></h5>
								<p class="text-justify"><?= $post->post_content; ?></p>
							</div>
						</div>
						<div class="row collapse">
							<div class="small-10 small-centered medium-12 columns">
								<dl>
									<dt>Liderança:</dt>
									<dd>
										<?php $posts = get_field('lideranca_rm'); $i = 1; if( $posts ): foreach( $posts as $p ): ?>
											<a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a><?php echo ( $i < sizeof($posts) ) ? ', ' : null ?>
										<?php $i++; endforeach; endif; ?>
									</dd>
									<dt>Reuniões:</dt>
									<dd><?= get_field( 'ensaios' ) ?></dd>
								</dl>
							</div>
						</div>
					</div>
				</div>

				<article>
					<div class="row collapse">
						<div class="small-10 small-centered medium-12 columns">
							<h2 class="blue uppercase">conheça nossas redes</h2>
							<ul class="small-block-grid-1 medium-block-grid-3">
								<li>
									<img data-src="holder.js/340x340/gray/text: Escolha ao lado uma rede com a qual mais se identifique e seja bem-vindo! :)" alt="">
								</li>
								<?php foreach ($posts_array as $rede): ?>
								<li>
									<a href="<?= get_permalink($rede->ID) ?>">
										<?= get_the_post_thumbnail( $rede->ID, 'large' ); ?>
										<div class="text-center">
											<span>
												<cite>
													<?= apply_filters( 'the_title', 'Rede de ' . $rede->post_title, $rede->ID ); ?>
													<small>veja mais</small>
												</cite>
											</span>
										</div>
									</a>
								</li>
								<?php endforeach ?>
							</ul>

						</div>
					</div>
				</article>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>