<?php
/**
 * Template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

			<div class="row">
			<?php if ( have_posts() ) : ?>
				<div class="small-10 small-centered medium-12 columns page-header">
					<h1><?php printf( __( 'Resultados para: %s', 'twentyeleven' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				</div>

				<div class="small-10 small-centered medium-12 columns">
					<ul class="small-block-grid-1 medium-block-grid-3" data-equalizer>
					<?php while ( have_posts() ) : the_post(); ?>
					<li data-equalizer-watch style="padding:0; background-size:cover; background-clip:content-box; background-repeat:no-repeat; background-position:50%; background-image: url(<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); echo $src[0]; ?>);">
						<a class="text-justify" style="display:block; height:100%; margin-top:-3px; padding:0 .75rem; background-color: rgba(255, 255, 255, 0.8)" href="<?php the_permalink(); ?>">
							<?php the_title( '<h3>', '</h3>', true ); ?>
							<span><?php the_excerpt(); ?></span>
						</a>
					</li>
					<?php endwhile; ?>
					</ul>
				</div>

			<?php else : ?>

				<div class="small-10 small-centered medium-12 columns">
					<article id="post-0" class="post no-results not-found">
						<header class="entry-header">
							<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentyeleven' ); ?></h1>
						</header><!-- .entry-header -->
					
						<div class="entry-content">
							<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyeleven' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					</article><!-- #post-0 -->
				</div>
			<?php endif; ?>
			</div>		

		<section id="primary">
			<div id="content" role="main">

			

			</div><!-- #content -->
		</section><!-- #primary -->
<?php get_footer(); ?>