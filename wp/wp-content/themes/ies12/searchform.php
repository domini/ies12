<form id="searchForm" class="columns" role="search" action="<?= home_url('/'); ?>" method="get">
	<div class="row collapse">
		<div class="small-11 small-centered medium-9 medium-push-1 columns">
			<div class="row collapse">
				<div class="small-11 columns">
					<input name="s" id="search" type="search" placeholder="Buscar no site..." value="<?php the_search_query(); ?>">
				</div>
				<div class="small-1 columns">
					<a href="#" class="button block greeny postfix icon-pesquisa"></a>
				</div>
			</div>
		</div>
	</div>
</form>