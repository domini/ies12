<?php
	$args        = array( 'post_type' => 'noticias' );
	$posts_array = get_posts( $args );
 	get_header();
	
 ?>
 		<div id="content" role="main">
			<section role="page" content="noticias">
				<?php get_template_part('partials/content', 'page-header'); ?>

				<div class="row">
					<div class="small-10 small-centered medium-8 medium-uncentered columns">
						<ul class="noticias-grid small-block-grid-1 medium-block-grid-2">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<li>
								<?= get_the_post_thumbnail($post->ID, 'large-thumb'); ?>
								<div class="small-11 small-centered columns">
									<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
									<span><?php the_excerpt(); ?></span>
								</div>
							</li>
							<?php endwhile; else : ?>
								<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
							<?php endif; ?>
						</ul>
					</div>

					<div class="small-10 small centered medium-4 medium-uncentered columns">
						<?php get_sidebar( get_post_type() ); ?>
					</div>
				</div>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer(); ?>