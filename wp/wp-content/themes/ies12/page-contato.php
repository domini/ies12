<?php get_header(); ?>
		<div id="content" role="main">
			<section role="page" content="noticia-single">
				<div class="page-header">
					<div class="row">
						<h2 class="icon-fone red uppercase"><?php the_title(); ?></h2>
					</div>
					<div data-interchange="[<?= get_template_directory_uri() ?>/images/backgrounds/bg-noticias.jpg, (default)]" alt=""></div>
					<div class="panel">
						<div class="row">
							<div class="small-10 small-centered columns">
								<ul data-orbit data-options="bullets:false; slide_number:false; timer:false;">
									<li data-orbit-slide="headline-1">
										<div>
											<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat distinctio maiores?</span>
										</div>
									</li>
									<li data-orbit-slide="headline-2">
										<div>
											<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae laboriosam odio nostrum optio dicta?</span>
										</div>
									</li>
									<li data-orbit-slide="headline-3">
										<div>
											<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae sequi fugiat, illo, perspiciatis.</span>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="small-10 small-centered medium-8 medium-uncentered columns contact-text">
					  <h2 class="red">Entre em contato conosco</h2>
					  <form id="cadastro" class="small-12 left" name="cadastro" method="POST" data-abide>
					      <div class="small-12">
					        <div id="alert-success" data-alert class="alert-box" style="display:none;">
								Mensagem enviada!
								<a href="#" class="close">&times;</a>
					        </div>
					        <div id="alert-error" data-alert class="alert-box alert" style="display:none;">
								Ocorreu um erro no envio.
								<a href="#" class="close">&times;</a>
					        </div>
					      </div>
					      <div class="small-12 row">
							<div class="small-2 columns">
								Nome:
							</div>
							<div class="small-10 columns">
						        <label for="nome">
						          <input id="nome" name="nome" type="text" size="100" maxlenght="100" required >
						          <small class="error">Você não preencheu o seu nome.</small>        
						        </label>
							</div>
					      </div>
					      <div class="small-12 row">
							<div class="small-2 columns">
								E-mail:
							</div>
							<div class="small-10 columns">
						        <label for="email">
						          <input id="email" name="email" type="email" size="100" maxlenght="100" required pattern="email">
						          <small class="error">Insira um e-mail válido.</small>
						        </label>
							</div>
					      </div>
					      <div class="small-12 row">
							<div class="small-2 columns">
								Telefone:
							</div>
							<div class="small-10 columns">
								<label for="telefone">
									<input id="telefone" name="telefone" type="text" required>
									<small class="error">Insira um telefone válido com o DDD. Exemplo: (11) 1234-5678</small>
								</label>
							</div>
					      </div>
					      <div class="small-12 row">
							<div class="small-2 columns">
								Igreja:
							</div>
							<div class="small-10 columns">
						        <label for="igreja">
						          <select name="igreja" id="igreja" required aria-invalid="true">
						            <option value=""></option>
						            <option value="igreja-1">Igreja 1</option>
						            <option value="igreja-2">Igreja 2</option>
						            <option value="igreja-3">Igreja 3</option>
						          </select>
						          <small class="error">Escolha uma das opções.</small>        
						        </label>
							</div>
					      </div>
					      <div class="small-12 row">
							<div class="small-2 columns">
								Assunto:
							</div>
							<div class="small-10 columns">
						        <label for="assunto">
						          <select name="assunto" id="assunto" required aria-invalid="true">
						            <option value=""></option>
						            <option value="Dúvidas">Dúvidas</option>
						            <option value="Elogios">Elogios</option>
						            <option value="Pedidos de Oração">Pedidos de Oração</option>
						            <option value="Problemas no site">Problemas no site</option>
						            <option value="Sugestões">Sugestões</option>
						          </select>
						          <small class="error">Escolha uma das opções.</small>        
						        </label>
							</div>
					      </div>
					      <div class="small-12 row">
							<div class="small-2 columns">
								Mensagem:
							</div>
							<div class="small-10 columns">
						        <label for="mensagem">
									<textarea placeholder="Mensagem" required></textarea>
									<small class="error">Escreva sua mensagem.</small>        
						        </label>
							</div>
					      </div>
					      <div class="small-12 row">
							<div class="small-2 columns"></div>
							<div class="small-10 columns">
						        <button name="enviar" type="submit" id="enviar">Enviar</button> 
						        <button name="limpar" type="reset" id="limpar">Limpar Campos</button> 
							</div>
					      </div>
					  </form>
					</div>

					<div class="small-10 small-centered medium-4 medium-uncentered columns">
						<?php get_sidebar( 'contato' ); ?>
					</div>
				</div>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer(); ?>