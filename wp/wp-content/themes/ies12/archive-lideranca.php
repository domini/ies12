<?php
	$args        = array( 'post_type' => 'lideranca', 'leadership' => 'pastor', 'posts_per_page' => -1, 'orderby' => 'title', 'order' => 'ASC' );
	$posts_array = new WP_Query( $args );
	
	get_header();
 ?>
		<div id="content" role="main">
			<section role="page" content="lideranca">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row collapse">
					<div class="small-10 small-centered medium-12 column">
						<ul class="small-block-grid-2 medium-block-grid-4">
							<?php if ( $posts_array->have_posts() ) : while ( $posts_array->have_posts() ) : $posts_array->the_post(); ?>
							<!-- post -->
							<li>
								<a href="<?php the_permalink();?>">
									<?php if (has_post_thumbnail()): the_post_thumbnail( 'large-thumb' ); ?>
										
									<?php endif ?>
									<strong class="uppercase"><?php the_title(); ?></strong>
									<p>Pastores | Equipe de 12</p>
								</a>
							</li>
							<?php endwhile; ?>
							<!-- post navigation -->
						</ul>
					</div>
					<?php else: ?>
					<!-- no posts found -->
					<h4>Nada para ver aqui =-(</h4>
					<?php endif; ?>
				</div>
			</section>
		</div>
		
		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>