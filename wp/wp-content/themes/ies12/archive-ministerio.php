<?php
	$args        = array( 'post_type' => 'ministerio' );
	$posts_array = get_posts( $args );
	
	get_header();
 ?>
		<div id="content" role="main">
			<section role="page" content="ministerios">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row">
					<div class="small-10 small-centered medium-12 columns text-columns text-justify">
						<h2 class="greeny uppercase">sobre os ministérios da igreja el shaddai</h2>
						<p>You guys aren't Santa! You're not even robots. How dare you lie in front of Jesus? You, a bobsleder!? That I'd like to see! You can't just have your characters announce how they feel. That makes me feel angry! We can't compete with Mom! Why, those are the Grunka-Lunkas! They work here in the Slurm factory. If rubbin' frozen dirt in your crotch is wrong, hey I don't wanna be right. Come, Comrade Bender! We must take to the streets! Bender, being God isn't easy.</p>
						<p class="lead greeny">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus amet eaque officiis fugiat vel, ab nisi, voluptatibus expedita aperiam quo. Impedit perspiciatis quaerat, vero nesciunt id porro commodi inventore reprehenderit.</p>
						<p>You guys aren't Santa! You're not even robots. How dare you lie in front of Jesus? You, a bobsleder!? That I'd like to see! You can't just have your characters announce how they feel. That makes me feel angry! We can't compete with Mom! Why, those are the Grunka-Lunkas! They work here in the Slurm factory. If rubbin' frozen dirt in your crotch is wrong, hey I don't wanna be right. Come, Comrade Bender! We must take to the streets! Bender, being God isn't easy. If you do too much, people get dependent on you, and if you do nothing, they lose hope. You have to use a light touch. Like a safecracker, or a pickpocket. Good news, everyone! There's a report on TV with some very bad news! I'm a thing. Hey, what kinda party is this? There's no booze and only one hooker.</p>
						<p>For example, if you killed your grandfather, you'd cease to exist! All I want is to be a monkey of moderate intelligence who wears a suit… that's why I'm transferring to business school! Of all the friends I've had… you're the first. That's a popular name today. Little "e", big "B"? Well I'da done better, but it's plum hard pleading a case while awaiting trial for that there incompetence. If rubbin' frozen dirt in your crotch is wrong, hey I don't wanna be right. Yes, if you make it look like an electrical fire. When you do things right, people won't be sure you've done anything at all. Hey, what kinda party is this? There's no booze and only one hooker. You guys aren't Santa! You're not even robots. How dare you lie in front of Jesus? Come, Comrade Bender! We must take to the streets! You lived before you met me?! Um, is this the boring, peaceful kind of taking to the streets? Kids have names? Doomsday device?</p>
					</div>
				</div>
			</section>

			<section role="ministerios-slider" class="greeny block">
				<div class="row">
					<div class="small-10 small-centered medium-12 columns">
						<h2 class="icy uppercase">conheça nossos ministérios:</h2>
						<div class="ministerios-slider">
							<?php foreach ($posts_array as $ministerio): ?>
							<?php $capa = get_post_meta($ministerio->ID, 'cover_picture_attachment', true); $capa = $capa['url']; ?>

							<div>
								<a class="btn-ministerios" href="<?= get_permalink($ministerio->ID) ?>">
									<img width="98%" data-lazy="<?= $capa; ?>" alt="<?= $ministerio->post_name; ?>">
									<div>
										<span>ministério de</span>
										<span class="huge uppercase text-center"><?= $ministerio->post_title; ?></span>
										<small>veja mais</small>
									</div>
								</a>
							</div>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			</section>
		</div>
		
		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>