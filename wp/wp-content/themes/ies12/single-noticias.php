<?php
	get_header();
	$catsArray = get_the_terms($post->ID, 'news-category');
 ?>
		<div id="content" role="main">
			<section role="page" content="noticia-single">
				<?php get_template_part('partials/content', 'page-header'); ?>
				
				<div class="row">
					<div class="small-10 small-centered medium-8 medium-uncentered columns">
						<?php while ( have_posts() ) : the_post(); ?>
						<?php setPostViews(get_the_ID()); ?>
						<?php the_post_thumbnail(); ?>
						<h2 class="bluey"><?php the_title(); ?>
							<small><?php get_the_category($post->ID); ?></small>
						</h2>
						<em id="postInfo"><?= the_tags('', '&middot;' ); ?>  | <?php the_author(); ?> | <?php the_date(); ?></em>
						
						<article class="text-justify"><?php the_content(); ?></article>

						<div class="row collapse appends">
							<h5 class="blue block uppercase">sobre o autor</h5>
							<div class="small-3 columns">
								<img data-src="holder.js/130x130/gray/text: Profile \n Picture" alt="">
							</div>
							<div class="small-9 columns">
								<span class="uppercase">pr carlos henrique trote</span><br>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas culpa perspiciatis commodi repellat similique sint, ipsum dolore quis! Nihil inventore eius amet, veritatis asperiores dignissimos ex vero! Vero, assumenda quaerat!
							</div>
						</div>

						<div class="row collapse appends">
							<h5 class="blue block uppercase">você vai gostar também de</h5>
							<ul class="small-block-grid-2 medium-block-grid-4">
								<?php $related_query = new WP_Query(array('post_type' => 'noticias', 'posts_per_page' => 4, 'orderby' => 'rand')); ?>
								<?php if ( $related_query->have_posts() ) : while ( $related_query->have_posts() ) : $related_query->the_post(); ?>
								<!-- post -->
								<li>
									<a href="<?= the_permalink(); ?>">
										<?= the_post_thumbnail( 'thumbnail' ); ?>
										<span><?= the_title(); ?></span>
									</a>
								</li>
								<?php endwhile; ?>
								<?php wp_reset_postdata(); ?>
								<!-- post navigation -->
								<?php else: ?>
								<h4>:(</h4>
								<?php endif; ?>
							</ul>
						</div>

						<div class="row collapse appends">
							<h5 class="blue block uppercase">comente</h5>
							<div id="disqus_thread"></div>
							<script type="text/javascript">
							/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
							var disqus_shortname = 'ies12'; // required: replace example with your forum shortname
							
							/* * * DON'T EDIT BELOW THIS LINE * * */
							(function() {
							var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
							dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
							(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
							})();
							</script>
							<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
						</div>
						<?php endwhile; // end of the loop. ?>
					</div>

					<div class="small-10 small-centered medium-4 medium-uncentered columns">
						<?php get_sidebar( get_post_type() ); ?>
					</div>
				</div>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer(); ?>