<?php

	if (isset($_GET['igreja']) && !empty($_GET['igreja'])) {
		$args = array( 'post_type' => 'igrejas', 's' => $_GET['igreja'] );
	} elseif (isset($_GET['tipo']) && !empty($_GET['tipo'])) {
		$args = array( 'post_type' => 'igrejas', 'church-category' => $_GET['tipo'] );
	} else {
		$args = array( 'post_type' => 'igrejas' );
	}
	
	$posts_array = get_posts( $args );
	
	get_header();
 ?>
		<div id="content" role="main">
			<section role="page" content="igrejas">
				<?php get_template_part('partials/content', 'page-header' ); ?>


				<div class="row">
					<form role="search" action="<?= get_post_type_archive_link( 'igrejas' ); ?>" method="get">
						<div class="row collapse">
							<div class="small-10 small-centered medium-4 medium-uncentered columns">
								<div class="row collapse">
									<div class="small-10 columns">
										<input name="igreja" id="search" type="search" placeholder="Buscar no site..." value="<?php the_search_query(); ?>">
									</div>
									<div class="small-2 columns">
										<input type="submit" class="button block orange postfix icon-pesquisa" value="Buscar">
										<!-- <a href="#" class="button block yellow postfix icon-pesquisa"></a> -->
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>

				<div class="row collapse">
					<div class="small-10 small-centered medium-12 columns" data-equalizer>
						<ul class="igrejas-grid small-block-grid-1 medium-block-grid-3">
							<?php foreach ($posts_array as $igreja): ?>
							<li data-equalizer-watch>
								<?php if (has_post_thumbnail( $igreja->ID )) echo get_the_post_thumbnail( $igreja->ID, 'large-thumb' ); ?>
								<a class="text-center" href="<?= get_permalink($igreja->ID) ?>">
									<strong class="huge uppercase text-center"><?= $igreja->post_title; ?></strong>
									<p>
										<span><?php $endereco = get_post_meta( $igreja->ID, 'endereco_completo' ); echo ( !empty($endereco[0]) ) ? $endereco[0] : null; ?></span>
									</p>
								</a>
							</li>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			</section>
		</div>
		
		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>