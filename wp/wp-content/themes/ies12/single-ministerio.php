<?php get_header() ?>
<?php
	// $lideranca   = get_post_meta($post->ID, 'lideranca', true);
	// $ensaios     = get_post_meta($post->ID, 'essay_final_format', true);
	$pic         = get_post_meta($post->ID, 'content_picture_attachment', true);
	$pic         = $pic['url'];
	
	$allMetas    = get_post_meta($post->ID);
	
	$args        = array( 'post_type' => 'ministerio' );
	$posts_array = get_posts( $args );

 ?>
		<div id="content" role="main">	
			<section role="page" content="single-ministerios">
				<?php get_template_part('partials/content', 'page-header' ); ?>

				<div class="row">
					<div class="medium-6 columns">
						<img src="<?= $pic; ?>" alt="<?= $post->post_name; ?>">
					</div>

					<div class="medium-6 columns">
						<div class="row collapse">
							<div class="small-12 column">
								<h5 class="greeny">Ministério de <?= $post->post_title; ?></h5>
								<p class="text-justify"><?= $post->post_content; ?></p>
							</div>
						</div>
						<div class="row collapse">
							<div class="small-10 small-centered medium-12 columns">
								<dl>
									<dt>Liderança:</dt>
									<dd>
										<?php $posts = get_field('lideranca_rm'); $i = 1; if( $posts ): foreach( $posts as $p ): ?>
											<a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a><?php echo ( $i < sizeof($posts) ) ? ', ' : null ?>
										<?php $i++; endforeach; endif; ?>
									</dd>
									<dt>Reuniões:</dt>
									<dd><?= get_field( 'ensaios' ) ?></dd>
								</dl>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section role="ministerios-slider" class="greeny block">
				<div class="row">
					<div class="small-10 small-centered medium-12 columns">
						<h2 class="icy uppercase">conheça nossos ministérios:</h2>
						<div class="ministerios-slider">
							<?php foreach ($posts_array as $ministerio): ?>
							<?php $capa = get_post_meta($ministerio->ID, 'cover_picture_attachment', true); $capa = $capa['url']; ?>

							<div>
								<a class="btn-ministerios" href="<?= get_permalink($ministerio->ID) ?>">
									<img width="98%" data-lazy="<?= $capa; ?>" alt="<?= $ministerio->post_name; ?>">
									<div>
										<span>ministério de</span>
										<span class="huge uppercase text-center"><?= $ministerio->post_title; ?></span>
										<small>veja mais</small>
									</div>
								</a>
							</div>
							<?php endforeach ?>
						</div>
					</div>
				</div>
			</section>
		</div>

		<?php get_template_part('partials/content', 'programacao'); ?>
<?php get_footer() ?>