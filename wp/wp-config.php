<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
// define('DB_NAME', 'wp_ies12');
define('DB_NAME', getenv('DB_NAME') ? getenv('DB_NAME') : 'localhost');

/** MySQL database username */
// define('DB_USER', 'root');
define('DB_USER', getenv('DB_USER') ? getenv('DB_USER') : 'sudo');

/** MySQL database password */
// define('DB_PASSWORD', 'gUFSkdh8');
define('DB_PASSWORD', getenv('DB_PASSWORD') ? getenv('DB_PASSWORD') : 'mysuperpassword');


/** MySQL hostname */
// define('DB_HOST', 'localhost');
define('DB_HOST', getenv('DB_HOST') ? getenv('DB_HOST') : 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'c/Pu6=@p)|r&mM14a6 e[QJ!5i$Z^h#J6uYwr$=8-_sKCjn^U&Pi`dd%jgn-V[OS');
define('SECURE_AUTH_KEY',  'zi5sN3ceOQ${@gD|a3ct;.NHk(V+=6:u7Q~w}fKfO|)wDXdU uLU&l[J$I -KYFV');
define('LOGGED_IN_KEY',    '`V]!sl=hV2Q-,o=4bQVsAHDc$D,wDyki8<kaz ~ ?YN48-7tu@7fbr2H>#8O7)_0');
define('NONCE_KEY',        'd*x:2k|9+@pN=I~9&Sfzos2:Je.Nx!s~`{c>9&JY-35W:) 4E?vO-N~[8(vFD)SB');
define('AUTH_SALT',        'rhli/DCGRV^eI1&&N(J@+OK}on;,*#1@T1JF+D)XSHu0},)^*VWX|m[%^YwOZ8gL');
define('SECURE_AUTH_SALT', '=KKjwJLfZ79D#xm|_pxobSU}.t-0PF9f|^e-F.z>lu-=q;!Tfm,XNJ[:N*kAAi?K');
define('LOGGED_IN_SALT',   't!,-=MYqsMx,haE4-xp,2K^xh}-^i+yxJ~ke|UEsl6>=*Mk]k}pED/dPmb9p6Qkz');
define('NONCE_SALT',       '9|zH4!4+!gb</t4>5al?5]5ABekN}U+>aY3K!vde7ZX+B`Hvn)G=`{j*_udWL^j.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
